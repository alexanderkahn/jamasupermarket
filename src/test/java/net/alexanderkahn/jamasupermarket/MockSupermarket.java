package net.alexanderkahn.jamasupermarket;

/**
 * Created by akahn on 12/14/14.
 * Supermarket class used for testing with mock data from the MockInventoryManager class
 */
public class MockSupermarket extends Supermarket{
    private InventoryManager inventoryManager;

    public MockSupermarket(Product[] products, ProductDiscount[] discounts) {
        this.inventoryManager = new MockInventoryManagerImpl(products, discounts);
    }

    @Override
    public InventoryManager getInventoryManager() {
        return inventoryManager;
    }
}
