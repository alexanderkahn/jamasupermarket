package net.alexanderkahn.jamasupermarket;

import java.math.BigDecimal;

/**
 * Created by akahn on 12/16/14.
 */
public class ProductDiscountTest {
    public void test() {
        productDiscount_gettersAndSetters();
        productDiscount_calculateDiscount_respectsIsDiscountUnmatchedItems();
        productDiscount_calculateDiscount_returnsDifferenceNotSubtotal();
    }

    private void productDiscount_gettersAndSetters() {
        ProductDiscount discount = new ProductDiscount(1, 10, new BigDecimal(.10), true);

        //getters
        assert discount.getReferenceId() == 1;
        assert discount.getDiscountQuantity() == 10;
        assert discount.getDiscountPercentPerItem().equals(new BigDecimal(.10));
        assert discount.isDiscountUnmatchedItems() == true;

        //setters
        discount.setReferenceId(2);
        discount.setDiscountQuantity(20);
        discount.setDiscountPercentPerItem(new BigDecimal(.20));
        discount.setDiscountUnmatchedItems(false);
        assert discount.getReferenceId() == 2;
        assert discount.getDiscountQuantity() == 20;
        assert discount.getDiscountPercentPerItem().equals(new BigDecimal(.20));
        assert discount.isDiscountUnmatchedItems() == false;
    }

    private void productDiscount_calculateDiscount_respectsIsDiscountUnmatchedItems() {
        ProductDiscount discount = new ProductDiscount(1, 10, new BigDecimal(0.5), false);

        //IsDiscountUnmatchedItems is false, so the 11th item should not receive a discount
        assert discount.calculateDiscount(new BigDecimal(10), 11).intValue() == 50;

        discount.setDiscountUnmatchedItems(true);
        //11th item will now be discounted
        assert discount.calculateDiscount(new BigDecimal(10), 11).intValue() == 55;
    }

    private void productDiscount_calculateDiscount_returnsDifferenceNotSubtotal(){
        ProductDiscount discount = new ProductDiscount(1, 10, new BigDecimal(.1), true);
        assert discount.calculateDiscount(new BigDecimal(10), 10).intValue() == 10;
    }
}
