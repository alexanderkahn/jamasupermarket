package net.alexanderkahn.jamasupermarket;

import java.math.BigDecimal;

/**
 * Created by akahn on 12/16/14.
 */
public class ProductTest {
    public void test() {
        //Product is a simple class for now, so just doing a little sanity check and moving on
        product_gettersAndSetters();
    }

    private static void product_gettersAndSetters() {
        Product product = new Product(1, new BigDecimal(10));

        //getters
        assert product.getProductCode() == 1;
        assert product.getPriceEach().equals(new BigDecimal(10));

        //setters
        product.setPriceEach(new BigDecimal(20));
        product.setProductCode(2);
        assert product.getProductCode() == 2;
        assert product.getPriceEach().equals(new BigDecimal(20));

        //equals
        assert new Product(2, new BigDecimal(20)).equals(product);
        assert !(new Product(2, new BigDecimal(100)).equals(product));
    }
}
