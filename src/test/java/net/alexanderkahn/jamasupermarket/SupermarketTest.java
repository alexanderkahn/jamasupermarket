package net.alexanderkahn.jamasupermarket;

import java.math.BigDecimal;

/**
 * Created by akahn on 12/16/14.
 */
public class SupermarketTest {
    public void test() {
        supermarketPrompt_checkout_240();
        supermarket_checkout_correctTotals();
    }

    private static void supermarketPrompt_checkout_240() {
        Supermarket market = new Supermarket();
        int checkoutResult = market.checkout("ABBACBBAB");
        assert checkoutResult == 240;
    }

    private static void supermarket_checkout_correctTotals() {
        Product[] products = {
                new Product('1', new BigDecimal(10)),
                new Product('2', new BigDecimal(20)),
                new Product('3', new BigDecimal(50)),
                new Product('4', new BigDecimal(500))
        };

        ProductDiscount[] discounts = {
                new ProductDiscount('1', 10, new BigDecimal(0.5), false),
                new ProductDiscount('2', 1, new BigDecimal(0.1), false),
                new ProductDiscount('4', 5, new BigDecimal(0.95), true),
                new ProductDiscount('7', 10, new BigDecimal(0.66), false)
        };

        MockSupermarket market = new MockSupermarket(products, discounts);
        assert market.checkout("11111") == 50;
        assert market.checkout ("444444") == 150;
        assert market.checkout("1234") == 578;
        assert market.checkout("111111111114") == 560;
    }
}
