package net.alexanderkahn.jamasupermarket;

import java.math.BigDecimal;

/**
 * Created by akahn on 12/16/14.
 */
public class ShoppingCartTest {
    public void test() {
        shoppingCart_add_existentItem();
        shoppingCart_add_nonexistentItem();
        shoppingCart_add_rejectsNullProduct();
        shoppingCart_add_rejectsZeroOrNegativeQuantity();
        shoppingCart_remove_nonexistentItem();
        shoppingCart_remove_partQuantity();
        shoppingCart_removeAll_fullQuantity();
    }

    private static void shoppingCart_add_nonexistentItem() {
        ShoppingCart cart = new ShoppingCart();
        Product product = new Product(1, new BigDecimal(1));
        assert cart.getQuantity(product) == 0;
        cart.add(product);
        assert cart.getQuantity(product) == 1;
    }

    private static void shoppingCart_add_existentItem() {
        ShoppingCart cart = new ShoppingCart();
        Product product = new Product(1, new BigDecimal(1));
        cart.add(product, 4);
        assert cart.getQuantity(product) == 4;
        cart.add(product);
        assert cart.getQuantity(product) == 5;
    }

    //Generates a warning
    private static void shoppingCart_add_rejectsNullProduct() {
        ShoppingCart cart = new ShoppingCart();
        Product product = null;
        assert cart.getTotalQuantity() == 0;
        cart.add(product);
        assert cart.getTotalQuantity() == 0;
    }

    //Generates two warnings
    private static void shoppingCart_add_rejectsZeroOrNegativeQuantity() {
        ShoppingCart cart = new ShoppingCart();
        Product product = new Product(1, new BigDecimal(5));
        assert cart.getTotalQuantity() == 0;
        cart.add(product, 0);
        assert cart.getTotalQuantity() == 0;
        cart.add(product, -1);
        assert cart.getTotalQuantity() == 0;
    }

    private static void shoppingCart_remove_partQuantity() {
        ShoppingCart cart = new ShoppingCart();
        Product product = new Product(1, new BigDecimal(1));
        cart.add(product, 4);
        assert cart.getQuantity(product) == 4;
        cart.remove(product);
        assert cart.getQuantity(product) == 3;
    }

    private static void shoppingCart_removeAll_fullQuantity() {
        ShoppingCart cart = new ShoppingCart();
        Product product = new Product(1, new BigDecimal(1));
        cart.add(product, 4);
        assert cart.getQuantity(product) == 4;
        cart.removeAll(product);
        assert cart.getQuantity(product) == 0;
    }

    //This test generates a warning
    private static void shoppingCart_remove_nonexistentItem() {
        ShoppingCart cart = new ShoppingCart();
        Product product = new Product(1, new BigDecimal(1));
        cart.add(product, 4);
        assert cart.getTotalQuantity() == 4;
        cart.remove(new Product(2, new BigDecimal(2)));
        assert cart.getTotalQuantity() == 4;
    }
}
