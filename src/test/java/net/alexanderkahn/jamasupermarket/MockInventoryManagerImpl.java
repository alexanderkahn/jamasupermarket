package net.alexanderkahn.jamasupermarket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by akahn on 12/14/14.
 * InventoryManager implementation that allows for different Products and Discounts than the prompt provides, for testing
 * of extra functionality.
 */
public class MockInventoryManagerImpl implements InventoryManager{

    private Product[] products;
    private Discount[] discounts;

    public MockInventoryManagerImpl(Product[] products, Discount[] discounts) {
        this.products = products;
        this.discounts = discounts;
    }

    @Override
    public Product lookupProduct(long productCode) {
        for (Product prod : this.products) {
            if (prod.getProductCode() == productCode) {
                return prod;
            }
        }

        return null;
    }

    @Override
    public Discount[] lookupDiscountsForProduct(Product product) {
        List<Discount> discountResults = new ArrayList<Discount>();
        for (Discount disc : this.discounts) {
            if (disc.appliesToProduct(product)) {
                discountResults.add(disc);
            }
        }

        return discountResults.toArray(new Discount[discountResults.size()]);
    }
}
