package net.alexanderkahn.jamasupermarket;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by akahn on 12/12/14.
 * For now, we're storing and reading our list of Products in a text file. This is likely to change as the product matures.
 */
public class Supermarket {

    private InventoryManager inventoryManager;

    public Supermarket() {
        inventoryManager = new FileInventoryManagerImpl();
    }

    //protected so it can be overridden for testing
    protected InventoryManager getInventoryManager() {
        return this.inventoryManager;
    }

    public int checkout(String items) {
        ShoppingCart cart = new ShoppingCart();

        for (char code :  items.toCharArray()) {
            cart.add(getInventoryManager().lookupProduct(code));
        }

        //The prompt asks for an int value (representing dollars) to be returned. Calculating money is more precise using
        //BigDecimal, so the conversion to int is done as late as possible to minimize impact of a possible switch
        //later in development.
        return this.calculateTotal(cart).setScale(0, RoundingMode.HALF_UP).intValue();
    }

    private BigDecimal calculateTotal(ShoppingCart cart) {
        BigDecimal total = new BigDecimal(0);
        for (Product prod : cart.getProducts()) {
            total = total.add(calculateItemSubtotal(prod, cart.getQuantity(prod)));
        }

        return total;
    }

    private BigDecimal calculateItemSubtotal(Product product, int quantity) {
        Discount[] discounts = getInventoryManager().lookupDiscountsForProduct(product);
        BigDecimal subtotal = product.getPriceEach().multiply(new BigDecimal(quantity));
        if (discounts != null && discounts.length > 0) {
            BigDecimal discountSubtotal = new BigDecimal(0);
            //for now, there is no validation around multiple discounts applied to the same item.
            for (Discount disc : discounts) {
                discountSubtotal = discountSubtotal.add(disc.calculateDiscount(product.getPriceEach(), quantity));
            }
            subtotal = subtotal.subtract(discountSubtotal);
        }
        return subtotal;
    }


}
