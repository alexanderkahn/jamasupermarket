package net.alexanderkahn.jamasupermarket;

/**
 * Created by akahn on 12/13/14.
 */
public interface InventoryManager {
    public Product lookupProduct(long productCode);
    public Discount[] lookupDiscountsForProduct(Product product);
}
