package net.alexanderkahn.jamasupermarket;

import com.sun.javaws.exceptions.InvalidArgumentException;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Created by akahn on 12/13/14.
 * To keep things simple with a small sample data set we're reading out of a few CSV files right now. This method will
 * likely need to be replaced as inventory grows and we need to track more data, or if we want to ensure uniqueness etc.
 */
public class FileInventoryManagerImpl implements InventoryManager {

    private Logger logger;

    public FileInventoryManagerImpl() {
        this.logger = Logger.getLogger(FileInventoryManagerImpl.class.getName());
    }

    @Override
    public Product lookupProduct(long productCode) {
        Scanner scanner = getFileScanner("products.csv");
        if (scanner == null) { return null; }

            while (scanner.hasNextLine()) {
                try {
                    Product prod = readProductFromString(scanner.nextLine());
                    if (prod.getProductCode() == productCode) {
                        return prod;
                    }
                }
                catch(Exception e) {
                    this.logger.log(Level.WARNING, e.getMessage(), e);
                }
            }
            return null;
    }

    @Override
    public Discount[] lookupDiscountsForProduct(Product product) {
        Scanner scanner = getFileScanner("discounts.csv");
        if (scanner == null) { return null; }

        List<Discount> discounts = new ArrayList<Discount>();
        while (scanner.hasNextLine()) {
            try {
                Discount disc = readDiscountFromString(scanner.nextLine());
                if(disc.appliesToProduct(product)) {
                    discounts.add(disc);
                }
            }
            catch(Exception e) {
                this.logger.log(Level.WARNING, e.getMessage(), e);
            }

        }
        return discounts.toArray(new Discount[discounts.size()]);
    }

    private Product readProductFromString(String s) throws IllegalArgumentException {
        String[] prodData = s.split(",");
        if (prodData.length != 2) {
            throw new IllegalArgumentException("Malformed Product data on line starting: " + prodData[0]);
        }

        return new Product((long)(prodData[0].charAt(0)), new BigDecimal(prodData[1]));
    }

    //this method will need to be changed to accomodate for different types of discount
    private Discount readDiscountFromString(String s) throws InvalidArgumentException{
        String[] discData = s.split(",");
        if (discData.length != 4) {
            throw new IllegalArgumentException("Malformed Discount data in file starting with: " + discData[0]);
        }
        return new ProductDiscount((long)discData[0].charAt(0), Integer.parseInt(discData[1]),
                    new BigDecimal(discData[2]), Boolean.parseBoolean(discData[3]));
    }

    private Scanner getFileScanner(String resourceName) {
        try {
            URL url = getClass().getResource(resourceName);
            return new Scanner(new File(url.getPath()));
        }
        catch (Exception e) {
            this.logger.log(Level.SEVERE, "Could not find resource: " + resourceName, e);
            return null;
        }
    }
}
