package net.alexanderkahn.jamasupermarket;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by akahn on 12/12/14.
 */
public class Product {
    //The prompt identifies products by character, but storing and retrieving by integer value makes it easier to
    //add a wider range of products, store uniquely in a database, etc.
    private long productCode;
    private BigDecimal priceEach;

    private Logger logger;

    public Product(long productCode, BigDecimal priceEach) {
        this.setProductCode(productCode);
        this.setPriceEach(priceEach);

        this.logger = Logger.getLogger(Product.class.getName());
    }

    public long getProductCode() {
        return this.productCode;
    }

    public void setProductCode(long productCode) {
        this.productCode = productCode;
    }

    public BigDecimal getPriceEach() {
        return priceEach;
    }

    public void setPriceEach(BigDecimal priceEach) {
        try {
            if (priceEach.compareTo(new BigDecimal(0)) < 0) {
                throw new IllegalArgumentException("Illegal price: " + priceEach.toString());
            }
            this.priceEach = priceEach;
        }
        catch (IllegalArgumentException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)  { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Product product = (Product) o;

        if (productCode != product.productCode) { return false; }
        if (priceEach != null ? !priceEach.equals(product.priceEach) : product.priceEach != null)  { return false; }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) this.getProductCode();
        result = 31 * result + (this.getPriceEach() != null ? this.getPriceEach().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return Long.toString(this.getProductCode());
    }
}