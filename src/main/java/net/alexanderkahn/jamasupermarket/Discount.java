package net.alexanderkahn.jamasupermarket;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by akahn on 12/14/14.
 * For now, discounts only exist per Product. Eventually, there may be discounts for department, brand, etc, whole purchase, etc.
 * I did get a bit carried away here and add the discountUnmatchedItems flag, which was not necessary given the prompt.
 * If hired, I hereby promise not to make up new features in Jama.
 */
public abstract class Discount {
    private long referenceId;
    private int discountQuantity;
    private BigDecimal discountPercentPerItem; // value from 0 - 1. Value represents percent of discount
    // (e.g. .25 means the item costs worth 75% of original price
    private boolean discountUnmatchedItems;    // if true, all items bought in quantities above the discountQuantity
    // will be discounted

    private Logger logger;

    public Discount(long referenceId, int discountQuantity, BigDecimal discountPercentPerItem, boolean discountUnmatchedItems) {
        this.setReferenceId(referenceId);
        this.setDiscountQuantity(discountQuantity);
        this.setDiscountPercentPerItem(discountPercentPerItem);
        this.setDiscountUnmatchedItems(discountUnmatchedItems);

        this.logger = Logger.getLogger(ProductDiscount.class.getName());
    }

    public long getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(long referenceId) { this.referenceId = referenceId; }

    public int getDiscountQuantity() {
        return discountQuantity;
    }

    public void setDiscountQuantity(int discountQuantity) {
        try {
            if (discountQuantity < 1) {
                throw new IllegalArgumentException("Illegal discount quantity: " + discountQuantity);
            }
            this.discountQuantity = discountQuantity;
        }
        catch (IllegalArgumentException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public BigDecimal getDiscountPercentPerItem() {
        return discountPercentPerItem;
    }

    public void setDiscountPercentPerItem(BigDecimal discountPercentPerItem) {
        try {
            if (discountPercentPerItem.compareTo(new BigDecimal(0)) < 0
                    || discountPercentPerItem.compareTo(new BigDecimal(1)) > 0) {
                throw new IllegalArgumentException("Illegal discount value: " + discountPercentPerItem);
            }
            this.discountPercentPerItem = discountPercentPerItem;
        }
        catch (IllegalArgumentException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public boolean isDiscountUnmatchedItems() {
        return discountUnmatchedItems;
    }

    public void setDiscountUnmatchedItems(boolean discountUnmatchedItems) {
        this.discountUnmatchedItems = discountUnmatchedItems;
    }

    public BigDecimal calculateDiscount(BigDecimal priceEach, int quantity) {
        int discountedItems;
        if (quantity < getDiscountQuantity()) { discountedItems = 0; }
        else if (isDiscountUnmatchedItems()) { discountedItems = quantity; }
        else { discountedItems = quantity - (quantity % getDiscountQuantity());
        }

        return priceEach.multiply(getDiscountPercentPerItem()).multiply(new BigDecimal(discountedItems));
    }

    public abstract boolean appliesToProduct(Product product);
}
