package net.alexanderkahn.jamasupermarket;

import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by akahn on 12/12/14.
 * Right now this class is essentially a Hashmap of Products and quantities. It may grow more complex over time as
 * shopping cart ID, date paid, customer, payment method etc are added.
 */
public class ShoppingCart {

    private HashMap<Product, Integer> products;
    private Logger logger;


    public ShoppingCart() {
        this.products = new HashMap<Product, Integer>();
        this.logger = Logger.getLogger(ShoppingCart.class.getName());
    }

    public Set<Product> getProducts() {
        return this.products.keySet();
    }

    public int getQuantity(Product product) {
        if (this.products.containsKey(product)) {
            return this.products.get(product);
        }
        else {
            return 0;
        }
    }

    public int getTotalQuantity(){
        int result = 0;
        for (Product product : this.getProducts()) {
            result += getQuantity(product);
        }
        return result;
    }

    public void add(Product product) { this.add(product, 1); }

    public void add(Product product, int quantity) {
        try {
            if (quantity < 1) { throw new IllegalArgumentException(String.format("%d is not a valid quantity", quantity)); }
            if (product == null) { throw new IllegalArgumentException("Product cannot be null"); }

            if (this.products.containsKey(product)) {
                this.products.put(product, this.products.get(product) + quantity);
            } else {
                this.products.put(product, quantity);
            }
        }
        catch (IllegalArgumentException e) {
            logger.log(Level.WARNING, e.getMessage(), e);

        }
    }

    public void remove(Product product) {
        this.remove(product, 1);
    }

    public void removeAll(Product product) {
        this.remove(product, this.getQuantity(product));
    }

    public void remove(Product product, int quantity) {
        try {
            if (product == null || !products.containsKey(product)) {
                throw new IllegalArgumentException(String.format("%s does not exist in the shopping cart", product.getProductCode()));
            }

            //Don't attempt to remove more items than exist in the cart
            if (quantity > this.getQuantity(product)) {
                this.logger.log(Level.INFO, String.format("Product %d: removing %d of %d items requested",
                        product.getProductCode(), this.getQuantity(product), quantity));
                quantity = this.getQuantity(product);
            }

            if (quantity >= this.getQuantity(product)) {
                this.products.remove(product);
            }
            else {
                this.products.put(product, this.getQuantity(product) - quantity);
            }
        }
        catch (IllegalArgumentException e) {
            this.logger.log(Level.WARNING, e.getMessage(), e);
        }
    }
}
