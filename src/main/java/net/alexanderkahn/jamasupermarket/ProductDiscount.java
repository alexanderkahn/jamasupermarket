package net.alexanderkahn.jamasupermarket;

import java.math.BigDecimal;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Created by akahn on 12/13/14.
 */
public class ProductDiscount extends Discount{

    public ProductDiscount(long referenceId, int discountQuantity, BigDecimal discountPercentPerItem, boolean discountUnmatchedItems) {
        super(referenceId, discountQuantity, discountPercentPerItem, discountUnmatchedItems);
    }

    @Override
    public boolean appliesToProduct(Product product) {
        if (this.getReferenceId() == product.getProductCode()) {
            return true;
        }
        else {
            return false;
        }
    }
}
